if not FixGit then FixGit = {} end
FixGit.FixPet = {}

local string_lower = string.lower

-- CareerResource.m_CurrentDisplay:UpdatePetBarAbilitiesFromAbilityList ()
--[[local oldAdd = PetWindow.AddNewAbilityProxy
PetWindow.AddNewAbilityProxy = function(newAbilityId)
	oldAdd(newAbilityId)
	EA_ChatWindow.Print(towstring(newAbilityId or "nil"))
end]]--

-- I don't think the alpha workaround is required anymore; let's wait for reports
--local isLoading

local function FadeInComponent (frame)		-- identical
	d(L"FadeInComponent")
    assert (frame)    
    frame:Show (true, Frame.FORCE_OVERRIDE)
    frame:StartAlphaAnimation (Window.AnimationType.SINGLE_NO_RESET, 0, PetWindow.BACKGROUND_ALPHA, PetWindow.FADE_IN_TIME, 0, 0)
	d(L"FadeInComponentDone")
end

local function FadeOutComponent (frame)		-- identical
	d(L"FadeOutComponent")
    assert (frame)
    frame:StartAlphaAnimation (Window.AnimationType.SINGLE_NO_RESET, PetWindow.BACKGROUND_ALPHA, 0, PetWindow.FADE_IN_TIME, 0, 0)
	d(L"FadeOutComponentDone")
end

local function StopComponentFade (frame)
	d(L"StopComponentFade")
    assert (frame)
    frame:StopAlphaAnimation ()
	--[[local alpha = WindowGetAlpha(frame:GetName ())
	if alpha < PetWindow.BACKGROUND_ALPHA then
		--if frame.m_FadeTimeRemaining then	-- use this to identify PetWindow out of the three
		--	frame.m_FadeTimeRemaining = PetWindow.FADE_IN_TIME
		--end
		frame:StartAlphaAnimation (Window.AnimationType.SINGLE_NO_RESET, alpha, PetWindow.BACKGROUND_ALPHA, PetWindow.FADE_IN_TIME, 0, 0)
	end]]--
    frame:Show (true)
	d(L"StopComponentFadeDone")
end

--[[local PlayerZoneId
local function ConditionallyReleasePet()
	if FrameManager.m_Frames.EA_CareerResourceWindow and FrameManager.m_Frames.EA_CareerResourceWindow.HasPet and FrameManager.m_Frames.EA_CareerResourceWindow:HasPet() then
		CommandPet (GameData.PetCommand.RELEASE)
	end
end

local function IsZoneCityViper(zoneId)
	return zoneId == 161 or zoneId == 178 or zoneId == 162 or zoneId == 198
end

local orig_Flight
local orig_JoinInstanceNow
local orig_SummoningAcceptPrompt
local function orig_EA_ChatWindow_OnKeyEnter(...)
	local input = WStringToString(EA_TextEntryGroupEntryBoxTextInput.Text)
	input = string_lower(input)
	local cmd = string.match(input, "^/%l+")
	if cmd == "/logout" or cmd == "/camp" then
		FixGit.FixPet.LogOutExit()
	elseif cmd == "/quit" or cmd == "/exit" or cmd == "/q" then
		FixGit.FixPet.LogOutExit()
	end
	oldEA_ChatWindow_OnKeyEnter(...)
end]]--
function FixGit.FixPet.Initialize()
	PetWindow.UpdatePet = function(self)
			d(L"UpdatePet")
			assert (self.m_UnitFrame)
			--if isLoading then return end
			self.m_UnitFrame:SetPlayersPetName (GameData.Player.Pet.name)    
			self.m_UnitFrame:UpdateLevel (GameData.Player.Pet.level)
			
			self:UpdatePetHealth()
			self:UpdatePetState()
			
			-- Fade In/Out the Window
			local hasPet    = self:HasPet ()
			local showing   = self:IsShowing ()
			
			if ((showing == false) and (hasPet == true))
			then
				self:SetPetCommands ()
				
				self.m_UnitFrame:SetPetPortrait()
				
				FadeInComponent (self)
				FadeInComponent (self.m_UnitFrame)
				FadeInComponent (self.m_Actionbar)
			elseif ((showing == true) and (hasPet == false))
			then
				FadeOutComponent (self)
				FadeOutComponent (self.m_UnitFrame)
				FadeOutComponent (self.m_Actionbar)
				
				self:ClearPetBarAbilities ()

				self.m_FadeTimeRemaining = PetWindow.FADE_IN_TIME
			elseif ((self.m_FadeTimeRemaining > 0) and (hasPet == true))
			then
				-- If the pet was just killed/desummoned/whatever and the player has the ability
				-- to re-summon the pet instantly, make sure to stop the windows alpha animation, and clear the update timer.    
				self.m_FadeTimeRemaining = 0
				
				self.m_UnitFrame:SetPetPortrait()
				
				StopComponentFade (self)
				StopComponentFade (self.m_UnitFrame)
				StopComponentFade (self.m_Actionbar)
			end
		end

	--[[RegisterEventHandler(SystemData.Events.SCENARIO_POST_MODE, "FixGit.FixPet.OnScenarioPostMode" )
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST,"FixGit.FixPet.StartCast")
	
	orig_Flight=EA_InteractionFlightMasterWindow.FlightPointLButtonUp
	EA_InteractionFlightMasterWindow.FlightPointLButtonUp = function()
			if IsZoneCityViper(GameData.Player.zone) then	-- City and Viper bug out no matter what
				ConditionallyReleasePet()
			else
				local destZone = WindowGetId( SystemData.ActiveWindow.name)
				if IsZoneCityViper(destZone) then
					ConditionallyReleasePet()
				else
					local newPairing = GetCampaignZoneData(destZone)
					if newPairing and newPairing.pairingId then
						if GetZonePairing() ~= newPairing.pairingId then
							ConditionallyReleasePet()
						end
					else
						ConditionallyReleasePet()
					end
				end
			end
			orig_Flight()
		end
		
	orig_JoinInstanceNow=EA_Window_ScenarioLobby.OnJoinInstanceNow
	EA_Window_ScenarioLobby.OnJoinInstanceNow = function()
			if( ButtonGetDisabledFlag("EA_Window_ScenarioJoinPromptBoxJoinNowButton" ) )	-- I prefer double checking this and be able to do a prehook instead of a replacing hook
			then
				return
			end
			ConditionallyReleasePet()
			orig_JoinInstanceNow()
		end

	orig_SummoningAcceptPrompt = EA_SummoningAcceptPrompt.OnAccept
	EA_SummoningAcceptPrompt.OnAccept = function()
			if( ButtonGetDisabledFlag( "EA_SummoningAcceptPromptBoxAcceptButton" ) )		-- I prefer double checking this and be able to do a prehook instead of a replacing hook
			then
				return
			end
			ConditionallyReleasePet()														-- TODO: Use EA_SummoningAcceptPrompt.ShowPrompt to check for pairing change
			orig_SummoningAcceptPrompt()
		end
	
	-- Logout/Exit
	RegisterEventHandler(SystemData.Events.LOG_OUT, "FixGit.FixPet.LogOutExit")
	RegisterEventHandler(SystemData.Events.EXIT_GAME, "FixGit.FixPet.LogOutExit")
	oldEA_ChatWindow_OnKeyEnter = EA_ChatWindow.OnKeyEnter
	EA_ChatWindow.OnKeyEnter = orig_EA_ChatWindow_OnKeyEnter]]--
	
end

--[[function FixGit.FixPet.OnScenarioPostMode()
	if ( ( GameData.ScenarioData.mode == GameData.ScenarioMode.POST_MODE ) and not GameData.Player.isInSiege ) then
		ConditionallyReleasePet()
	end
end


-- Funncy cast stuff for zoning via Book of Binding
-- Note to myself: Viper (Guild Scroll abilityId 4181) bugs even from chaos (tested: Praag)
-- Viper zone id: 178, City bugs out too no matter what: id 161 IC
local broadcastTime
function FixGit.FixPet.StartCast(abilityId,_,desiredCastTime)	--, isChannel,_, holdCastBar)
	if abilityId == 246 or abilityId == 4181 then
		--if abilityId == 246 then
			-- do something with GameData.Player.bindLocation
			-- however, the bindinglocations table seems on first sight kinda...not as organized as it could be
		--end
		broadcastTime = desiredCastTime - 0.3
		RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "FixGit.FixPet.EndCast")
		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "FixGit.FixPet.OnUpdateCast")
	end
end

function FixGit.FixPet.EndCast(isCancel)--, fromQueuedCall)
	if not isCancel then	-- shouldn't really happen, but you never know with this game
		ConditionallyReleasePet()
	end
	UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST, "FixGit.FixPet.EndCast")
	UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "FixGit.FixPet.OnUpdateCast")
end

function FixGit.FixPet.OnUpdateCast(elapsed)
	broadcastTime = broadcastTime - elapsed
	if broadcastTime <= 0 then
		ConditionallyReleasePet()
		UnregisterEventHandler(SystemData.Events.PLAYER_END_CAST, "FixGit.FixPet.EndCast")
		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "FixGit.FixPet.OnUpdateCast")
	end
end

--
-- Logout/Exit Event Handling
--
function FixGit.FixPet.LogOutExit() RegisterEventHandler( TextLogGetUpdateEventId( "System" ), "FixGit.FixPet.OnTextLogUpdated" ) end

function FixGit.FixPet.OnTextLogUpdated(updateType, filterType)
	if( updateType ~= SystemData.TextLogUpdate.ADDED ) then return end
	local msgcount = TextLogGetNumEntries("System")
	if msgcount == 0 then
		return
	end
	local msg = select(3, TextLogGetEntry("System", TextLogGetNumEntries("System") - 1))
	if msg == GetFormatStringFromTable("Hardcoded", 456, {5}) then
		ConditionallyReleasePet()
		UnregisterEventHandler( TextLogGetUpdateEventId( "System" ), "FixGit.FixPet.OnTextLogUpdated" )
	elseif msg == GetStringFromTable("Hardcoded", 457) then
		UnregisterEventHandler( TextLogGetUpdateEventId( "System" ), "FixGit.FixPet.OnTextLogUpdated" )
	end
end]]--