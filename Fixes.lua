if not FixGit then FixGit = {} end

-- Auction Window locals
local WINDOW_NAME = "AuctionWindow"
local SELL_CONTROLS_NAME = WINDOW_NAME.."SellControls"

-- Bank Window locals
local NUM_COLS = 8
local NUM_ROWS = 10
local NUM_SLOTS_PER_TAB = NUM_ROWS * NUM_COLS
-- those are for the MouseOver Fix, Mythic always choses locals exactly when it causes trouble and avoids it, when it's performance relevant
local NUM_BANK_SLOTS = 80
local function GetMinVisibleBankSlot()
    return (BankWindow.currentTabNumber-1) * NUM_SLOTS_PER_TAB + 1
end
local function GetMaxVisibleBankSlot()
    return BankWindow.currentTabNumber * NUM_SLOTS_PER_TAB
end

function FixGit.FixGlobalItemData()
	AuctionWindowSellControls.UpdateCreateButton = function()				-- replacing hooks, not nice but it's YOUR fault, Mythic
		local itemName = LabelGetText( SELL_CONTROLS_NAME.."ItemName")
		ButtonSetDisabledFlag( SELL_CONTROLS_NAME.."CreateButton", itemName == L"" )
	end
	
	BankWindow.SetBankSlotsInRange = function( min, max )					-- at least I'm fairly certain no one else hooks those, remember whose fault it is that it isn't fixed in the default ui.
		if( min < max and max - min <= NUM_SLOTS_PER_TAB )
		then
			for slot = min, max
			do
				local itemData = BankWindow.items[slot]
				BankWindow.SetBankSlot( slot, itemData )
			end
		end
	end
	
	EA_Window_Backpack.UpdateAllQuestItemSlots = function()					-- I think I have forgotten about whose fault it is, whose was it? 
		local questItems = DataUtils.GetQuestItems()
		if questItems ~= nil then
	   
			EA_Window_Backpack.DrawQuestItemsLayout()
	   
			for slot = 1, EA_Window_Backpack.numberOfSlots[EA_Window_Backpack.TYPE_QUEST] do
				local itemData = questItems[slot]
				EA_Window_Backpack.SetQuestBackpackSlot( EA_Window_Backpack.QUEST_SLOT_NAME_BASE..slot, itemData, false, nil )
			end
	   
		end
	end
end
local orig_BankWindow_OnNewBankSlots
function FixGit.FixBankWindowTooltip()
	-- for reasons beyond me, the SystemData.MouseOverWindow.name lags behind
	-- therefor fix the troublesome caller instead of BankWindow.EquipmentMouseOver
	NUM_BANK_SLOTS = GameData.Player.numBankSlots										-- at this point I should better have done a replacing mod. sigh.
	orig_BankWindow_OnNewBankSlots = BankWindow.OnNewBankSlots
	BankWindow.OnNewBankSlots = function()
			orig_BankWindow_OnNewBankSlots()
			NUM_BANK_SLOTS = GameData.Player.numBankSlots
		end
	BankWindow.UpdateBankSlot = function ( slot )
    
			-- make sure slot number is in proper range
			if slot < 1 or slot > NUM_BANK_SLOTS then
				 DEBUG(L"ERROR in BankWindow.UpdateBankSlot received out of range slot index = "..slot)
				return  
			end 
			
			local itemData  = GetSingleItem( GameData.ItemLocs.BANK, slot, false )
			
			BankWindow.items[slot] = itemData
			if( slot >= GetMinVisibleBankSlot()
				and slot <=  GetMaxVisibleBankSlot() ) --If the bank slot is visible then set it
			then
				BankWindow.SetBankSlot( slot, itemData )
			end

			-- If we are placing the item that is currently on the cursor, clear it
			if( Cursor.IconOnCursor() and itemData and (Cursor.Data.ObjectId == itemData.uniqueID or BankWindow.dropPending == true) ) then 
				Cursor.Clear()
			end
			
			-- If we are mousing over the updated slot, show the tooltip
			local SLOTS_NAME = BankWindow.GetSlotWindowForSlotNumber( slot )
			if( SystemData.MouseOverWindow.name == SLOTS_NAME ) then    
				--BankWindow.EquipmentMouseOver( BankWindow.GetButtonIndexForSlotNumber( slot ) )
				local buttonIndex = BankWindow.GetButtonIndexForSlotNumber( slot )
				local slot = BankWindow.GetSlotNumberForButtonIndex( buttonIndex )  
				local itemData = BankWindow.GetItem( slot ) 
				if not DataUtils.IsValidItem( itemData ) then
					Tooltips.ClearTooltip()
				else 
					Tooltips.CreateAndTintItemTooltip( itemData, 
													   SystemData.ActiveWindow.name.."SlotsButton"..buttonIndex,
													   Tooltips.ANCHOR_WINDOW_RIGHT, 
													   Tooltips.ENABLE_COMPARISON )
				end
			end
			
		end
end

function FixGit.FixGuildBannerMouseOver() StringTables.Guild.TOOLTIP_NO_PERMISSION = StringTables.Guild.TOOLTIP_BANNER_NO_PERMISSION end	-- sigh

function FixGit.FixSummoningGroupUpdate()
	EA_SummoningAcceptPrompt.OnGroupUpdated = function()
		if( WindowGetShowing( "EA_SummoningAcceptPrompt" ) )
		then
			WindowSetShowing("EA_SummoningAcceptPrompt", not (IsPlayerSolo()==1))	--your isPlayerSolo is nil
		end
	end
end

function FixGit.FixWorldMapTimerAnchoring()
	EA_Window_WorldMap.CreateObjectiveTimers = function ()
		local mapDisplay = "EA_Window_WorldMapZoneViewMapDisplay"
		local currentZoneId = EA_Window_WorldMap.currentMap
		if (currentZoneId == 0) then
			return
		end
		
		local objectivesData = nil
		if GlyphDisplay.DoesZoneHaveZoneControl( currentZoneId ) 
		then
			objectivesData = GetZoneObjectivesData( currentZoneId )
		end
		
		if (objectivesData ~= nil) then
			for _, objectivePoint in ipairs(objectivesData)
			do
				local objectiveTimerWindow = "ObjectiveTimer_"..currentZoneId.."_"..objectivePoint.objId
				
				local x, y = MapGetPointForCoordinates(mapDisplay, objectivePoint.objPositionX, objectivePoint.objPositionY)
				CreateWindowFromTemplate(objectiveTimerWindow, "ObjectiveMapTimer", mapDisplay)
				WindowSetId(objectiveTimerWindow, objectivePoint.objId)
				if x then	-- Split maps in T1-T3: x,y nil
					WindowAddAnchor(objectiveTimerWindow, "topleft", mapDisplay, "topleft", x, y)
				end
				if (objectivePoint.objMapTimer > 0) then
					if x then
						LabelSetText(objectiveTimerWindow, TimeUtils.FormatClock(objectivePoint.objMapTimer))
					end
					EA_Window_WorldMap.activeObjectiveTimers[objectivePoint.objId] = objectivePoint.objMapTimer
				else
					WindowSetShowing(objectiveTimerWindow, false)
					EA_Window_WorldMap.inactiveObjectiveTimers[objectivePoint.objId] = 0
				end
				
			end
		end
	end
end

-- your locking issue fix for 1.4.6
function FixGit.FixLELockingIssue()
	function LayoutControlFrame:BeginMoving()

		if( self.m_moving == true ) or self:GetActiveFrame().m_windowData.isLocked
		then
			return
		end
		self.m_moving = true
		WindowSetMoving( self:GetName(), true )
		
		self.m_snapFrame:SetActiveFrame(self)
	end
end

function FixGit.BypassIsBeingThrown()
	-- only checked against in lua it seems, well, as long as it doesn't work correctly isup it's a good thing haha
	
	-- scenariolobbywindow.lua
	function EA_Window_ScenarioLobby.UpdateLaunchingJoinButtons()

		-- Disable the 'Join Now' Button if the player is in combat & Show the description text.
		local flagValue = false
		if( GameData.Player.inCombat == true)-- or GameData.Player.isBeingThrown == true )
		then
			flagValue = true
		end
		
		ButtonSetDisabledFlag("EA_Window_ScenarioJoinPromptBoxJoinNowButton", flagValue )
		ButtonSetDisabledFlag("EA_Window_ScenarioStartingJoinNowButton", flagValue )
		
		WindowSetShowing("EA_Window_ScenarioStartingInCombatText", flagValue )
		WindowSetShowing("EA_Window_ScenarioJoinPromptBoxInCombatText", flagValue )

	end
	
	-- summoningprompt.lua
	function EA_SummoningAcceptPrompt.UpdateButtons()
		-- Disable the Accept Button if the player is in combat and show the description text.
		local flagValue = false
		if( GameData.Player.inCombat == true)-- or GameData.Player.isBeingThrown == true )
		then
			flagValue = true
		end
		
		ButtonSetDisabledFlag("EA_SummoningAcceptPromptBoxAcceptButton", flagValue )
		WindowSetShowing("EA_SummoningAcceptPromptBoxInCombatText", flagValue )
	end
	
	-- currenteventswindow.lua
	function EA_Window_CurrentEvents.HideOnCombat()
		if (GameData.Player.inCombat == true)-- or GameData.Player.isBeingThrown == true)
		then
			WindowSetShowing( "EA_Window_CurrentEvents", false )  
		end  
	end

	function EA_Window_CurrentEvents.Show()
		if (GameData.Player.inCombat == false)-- and GameData.Player.isBeingThrown == false)
		then
			WindowSetShowing( "EA_Window_CurrentEvents", true )  
		end  
	end

	function EA_Window_CurrentEvents.ToggleShowing()
		if (GameData.Player.inCombat == true)-- or GameData.Player.isBeingThrown == true)
		then
			EA_Window_CurrentEvents.HideOnCombat()
		else    
			WindowUtils.ToggleShowing( "EA_Window_CurrentEvents" )
		end
	end
end