<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

    <UiMod name="FixGit_ScenarioGroupWindow" version="1.3" date="06/09/2012" >
		<Replaces name="EA_ScenarioGroupWindow" />
        <Author name="EAMythic | fixes and enhancements by Wothor" email="" />
		<VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Description text="This module contains the EA Default scenario grouping interface components.<br>Current Enhancements: Better Group Hopping" />
        <Dependencies>        
            <Dependency name="EASystem_Utils" />
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="EATemplate_DefaultWindowSkin"  />
            <Dependency name="EASystem_Tooltips" />
			<Dependency name="EASystem_ResourceFrames" />   
			<Dependency name="EASystem_Strings" />   
			<Dependency name="EA_AlertTextWindow" />
			<Dependency name="EA_BattlegroupHUD" />
			<Dependency name="EA_GroupWindow" />
        </Dependencies>
        <Files>        
            <File name="Source/ScenarioGroupWindow.lua" />
            <File name="Source/ScenarioGroupWindow.xml" />
        </Files>
        <OnInitialize>
            <CreateWindow name="ScenarioGroupWindow" show="false" />
        </OnInitialize>         
        <SavedVariables>
            <SavedVariable name="ScenarioGroupWindow.GroupWindowSettings" />
        </SavedVariables>
    </UiMod>
    
</ModuleFile>    