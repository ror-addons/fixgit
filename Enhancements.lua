if not FixGit then FixGit = {} end

function FixGit.EnhancePetUnitFrame()
	local function emptyfunc() end
	
	local isOrigUserHidden
	local isDirty
	local function resizeEndCallback() isDirty = true end
	local function moveEndCallback() isDirty = true end
	LayoutEditor.RegisterWindow( "PetHealthWindow", L"PlayerPet", L"The unitframe for the players pet", false, false, true, nil, nil, nil, nil, resizeEndCallback, moveEndCallback )
		
	-- since the xml doesn't have savedsettings = true, we need to save it ourselves...-.-
	PetWindow.ApplyUnitFrameSettings = function(self)
		local frame = FrameManager:Get("PetHealthWindow")
		if frame then
			if LayoutEditor.IsWindowUserHidden("PetHealthWindow") then
				if frame:IsShowing () then
					frame:Show(false)
				end
				frame.Show = emptyfunc
				frame.StartAlphaAnimation = emptyfunc
				frame.StopAlphaAnimation = emptyfunc
			elseif (not LayoutEditor.IsWindowUserHidden("PetHealthWindow")) then
				frame.Show = nil
				frame.StartAlphaAnimation = nil
				frame.StopAlphaAnimation = nil
				FrameManager:Get("EA_CareerResourceWindow"):UpdatePet()
			end
			local Settings = FixGit.ProfileSettings and FixGit.ProfileSettings.PetUnitFrame
			if Settings then
				frame:SetScale (Settings.scale)
				frame:SetAnchor( {Point = Settings.point, RelativePoint = Settings.parentpoint, RelativeTo = Settings.parent, XOffset = Settings.x, YOffset = Settings.y} )
			end
		end
	end
	function PetWindow:ResetUnitFrame()
		local frame = FrameManager:Get("PetHealthWindow")
		frame:SetAnchor( {Point = "bottomright", RelativePoint = "topleft", RelativeTo = "PlayerWindowPortrait", XOffset = -42, YOffset = -8} )
		frame:SetScale(1)
		FixGit.ProfileSettings.PetUnitFrame = nil
	end

	local function LayoutEditorDone(event)
			if LayoutEditor.EDITING_BEGIN == event  then
				isOrigUserHidden = LayoutEditor.IsWindowUserHidden("PetHealthWindow")
			elseif LayoutEditor.EDITING_END == event then
				local needsUpdate = isDirty or (isOrigUserHidden ~= LayoutEditor.IsWindowUserHidden("PetHealthWindow"))
				if isDirty then
					isDirty = nil
					
					if not FixGit.ProfileSettings.PetUnitFrame then FixGit.ProfileSettings.PetUnitFrame = {} end
					local Settings = FixGit.ProfileSettings.PetUnitFrame

					Settings.scale = WindowGetScale("PetHealthWindow")
					
					local point, parentpoint, parent, x, y = WindowGetAnchor("PetHealthWindow", 1)
					Settings.point  = parentpoint	-- switch
					Settings.parentpoint = point	-- eroo?
					Settings.parent = parent
					Settings.x = x
					Settings.y = y
				end
				if needsUpdate then PetWindow:ApplyUnitFrameSettings() end
			end
		end
	LayoutEditor.RegisterEditCallback( LayoutEditorDone )
	
	local oldPetWindowCreate = PetWindow.Create
	PetWindow.Create = function (self,...)
			local petWindow = oldPetWindowCreate(self,...)
			petWindow:ApplyUnitFrameSettings()
			return petWindow
		end
	PetWindow:ApplyUnitFrameSettings()
end


--
-- Auction Linking. Credits to dnL7up.
--
local ItemLinkHook = nil
function FixGit.ItemLinkHook(ItemData)
	if ItemData and WindowGetShowing("AuctionWindow") then
		TextEditBoxSetText("AuctionWindowSearchControlsSearchBox", ItemData.name)
	else
		ItemLinkHook(ItemData)
	end
end
function FixGit.HookAuctionLink()
	if AuctionLink then -- original "mod" fix; but it has a hooking problem, therefor...
		EA_ChatWindow.Print(L"FixGit: Warning. AuctionLink is installed and may interfere with the correct auction linking enhancement. It's recommended to disable or uninstall it.")
	end
	ItemLinkHook = EA_ChatWindow.InsertItemLink
	EA_ChatWindow.InsertItemLink = FixGit.ItemLinkHook
end
function FixGit.UnHookAuctionLink()
	if AuctionLink then return end	-- original "mod" fix
	if EA_ChatWindow.InsertItemLink ~= FixGit.ItemLinkHook then
		EA_ChatWindow.Print(L"Warning. There's another hook interfering.")
	end
	EA_ChatWindow.InsertItemLink = ItemLinkHook
end
