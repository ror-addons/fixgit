if not FixGit then FixGit = {} end
local MAJOR = "FixGit"

function FixGit.Initialize()
	if not FixGit.ProfileSettings then FixGit.ProfileSettings = {} end
	FixGit.FixGlobalItemData()
	FixGit.FixBankWindowTooltip()
	FixGit.FixGuildBannerMouseOver()
	FixGit.FixSummoningGroupUpdate()
	FixGit.FixWorldMapTimerAnchoring()
	FixGit.FixLELockingIssue()
	FixGit.HookAuctionLink()
	FixGit.BypassIsBeingThrown()
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_LINE_UPDATED, MAJOR..".OnPlayerCareerLineUpdated")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, MAJOR..".OnPlayerCareerLineUpdated")
end

local isPetClass = {
	[GameData.CareerLine.WHITE_LION]=true,
	[GameData.CareerLine.MAGUS]=true,
	[GameData.CareerLine.ENGINEER]=true,
	[GameData.CareerLine.SQUIG_HERDER]=true
}
function FixGit.OnPlayerCareerLineUpdated()
	if isPetClass[GameData.Player.career.line] then
		--FixGit.FixPet.Initialize()
		FixGit.EnhancePetUnitFrame()
	end
end