<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

    <UiMod name="FixGit" version="1.7.4" date="08/19/2012" >
        <Author name="Wothor" email="" />
		<VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.1" />
        <Description text="Fixes Mythics bugs since they're just ignoring my reported fixes." />
        <Dependencies>
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="EASystem_LayoutEditor" />
            <Dependency name="EA_ActionBars" />
            <Dependency name="EA_MoraleWindow" />
            <Dependency name="EA_CareerResourcesWindow" />
            <Dependency name="EA_CastTimerWindow" />
            <Dependency name="EA_GrantedAbility" />
            <Dependency name="EA_TacticsWindow" />
			<Dependency name="EASystem_ActionBarClusterManager" />
			<Dependency name="EA_AuctionHouseWindow" />
			<Dependency name="EA_BankWindow" />
			<Dependency name="EA_BackpackWindow" />
			<Dependency name="EA_SummoningPrompt" />
			<Dependency name="EA_InteractionWindow" />
			<Dependency name="EA_ScenarioLobbyWindow" />
			<Dependency name="EA_WorldMapWindow" />
			<Dependency name="EA_SummoningPrompt" />
			<Dependency name="EA_CurrentEventsWindow" />
        </Dependencies>
        <Files>
			<File name="FixGit.lua" />
			<File name="Fixes.lua" />
            <File name="FixPet.lua" />
			<File name="Enhancements.lua" />
        </Files>
        <OnInitialize>
            <CallFunction name="FixGit.Initialize" />
        </OnInitialize> 
		<SavedVariables>
				<SavedVariable name="FixGit.ProfileSettings" />
		</SavedVariables>
		<WARInfo>
			<Categories>
				<Category name="ACTION_BARS" />
				<Category name="SYSTEM" />
			</Categories>
			<Careers>
				<Career name="SQUIG_HERDER" />
				<Career name="MAGUS" />
				<Career name="ENGINEER" />
				<Career name="WHITE_LION" />
			</Careers>
		</WARInfo>
    </UiMod>
</ModuleFile>
